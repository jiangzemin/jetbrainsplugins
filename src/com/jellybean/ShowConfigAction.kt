package com.jellybean

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.diagnostic.Logger

class ShowConfigAction : AnAction() {

    override fun actionPerformed(e: AnActionEvent) {
        val selector = ConfigSelector(e)
        selector.show()
    }
}