package com.jellybean;

import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import org.jetbrains.annotations.SystemIndependent;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConfigSelector {
    public static final String CATEGORY_NAME = "CF_CONFIG";
    private Project project;
    private JFrame frame = new JFrame("Club Factory 配置");
    private JPanel panel;
    private JComboBox boxEnvironment;
    private JComboBox boxChannel;
    private JButton button;
    private String currentEnvironment;
    private String currentChannel;
    private String propertiesContent;
    private AnActionEvent event;

    public ConfigSelector(AnActionEvent event) {
        this.project = event.getProject();
        this.event = event;
        if (project == null) {
            Logger.getInstance(CATEGORY_NAME).error("project 是空的");
            return;
        }
        setConfirmActionListener();
        panel.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(AncestorEvent event) {
                refreshConfig();
            }

            @Override
            public void ancestorRemoved(AncestorEvent event) {

            }

            @Override
            public void ancestorMoved(AncestorEvent event) {

            }
        });
    }

    private void setConfirmActionListener() {
        button.addActionListener(e -> {
            modifyProperties();
            frame.dispose();
        });
    }

    /**
     * 修改配置文件
     */
    private void modifyProperties() {
        String environment = (String) boxEnvironment.getSelectedItem();
        String channel = (String) boxChannel.getSelectedItem();
        boolean environmentChanged = !environment.equals(currentEnvironment);
        boolean channelChanged = !channel.equals(currentChannel);
        if (environmentChanged || channelChanged) {
            VirtualFile file = getPropertiesFile(project);
            if (file == null) {
                return;
            }
            String replacedContent;
            if (propertiesContent != null) {
                if (environmentChanged) {
                    Pattern pattern = Pattern.compile("ENVIRONMENT=.*");
                    Matcher environmentMatcher = pattern.matcher(propertiesContent);
                    replacedContent = environmentMatcher.replaceFirst("ENVIRONMENT=" + environment);
                    propertiesContent = replacedContent;
                }
                if (channelChanged) {
                    Pattern pattern = Pattern.compile("CHANNEL=.*");
                    Matcher channelMatcher = pattern.matcher(propertiesContent);
                    replacedContent = channelMatcher.replaceFirst("CHANNEL=" + channel);
                    propertiesContent = replacedContent;
                }
                WriteCommandAction.runWriteCommandAction(project, () -> {
                    try {
                        file.setBinaryContent(propertiesContent.getBytes());
                        syncGradle();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                });
            }
        }
    }

    private void syncGradle() {
        ActionManager am = ActionManager.getInstance();
        AnAction syncAction = am.getAction("Android.SyncProject");
        syncAction.actionPerformed(event);
    }

    private VirtualFile getPropertiesFile(Project project) {

        @SystemIndependent String projectBasePath = project.getBasePath();
        VirtualFile virtualFile = LocalFileSystem.getInstance().findFileByPath(projectBasePath + "/gradle.properties");
        if (virtualFile == null) {
            Logger.getInstance(CATEGORY_NAME).error("找不到属性文件：" + projectBasePath);
            return null;
        } else {
            return virtualFile;
        }
    }

    private void refreshConfig() {
        VirtualFile file = getPropertiesFile(project);
        if (file == null) {
            return;
        }
        file.refresh(true, true, () -> {
            propertiesContent = null;
            try {
                propertiesContent = new String(file.contentsToByteArray());
                System.out.println(propertiesContent);

            } catch (Exception e1) {
                e1.printStackTrace();
            }
            currentEnvironment = findEnvironment(propertiesContent);
            currentChannel = findChannel(propertiesContent);
            boxEnvironment.setSelectedItem(currentEnvironment);
            boxChannel.setSelectedItem(currentChannel);
        });
    }

    private String findEnvironment(String content) {
        return content.split("ENVIRONMENT=")[1].split("\n")[0];
    }

    private String findChannel(String content) {
        return content.split("CHANNEL=")[1].split("\n")[0];
    }


    public void show() {
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(frame.getParent());
        frame.setVisible(true);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel = new JPanel();
        panel.setLayout(new GridLayoutManager(3, 2, new Insets(10, 10, 10, 10), -1, -1));
        panel.setMinimumSize(new Dimension(170, 105));
        panel.setPreferredSize(new Dimension(180, 160));
        final JLabel label1 = new JLabel();
        label1.setText("环境");
        panel.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("渠道");
        panel.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        boxEnvironment = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        defaultComboBoxModel1.addElement("online");
        defaultComboBoxModel1.addElement("online_https");
        defaultComboBoxModel1.addElement("pre");
        defaultComboBoxModel1.addElement("pre2");
        defaultComboBoxModel1.addElement("pre3");
        defaultComboBoxModel1.addElement("test_app");
        defaultComboBoxModel1.addElement("test");
        boxEnvironment.setModel(defaultComboBoxModel1);
        panel.add(boxEnvironment, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(30, 20), new Dimension(100, 40), new Dimension(150, 60), 0, false));
        boxChannel = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
        defaultComboBoxModel2.addElement("googlePlay");
        defaultComboBoxModel2.addElement("xiaomi");
        defaultComboBoxModel2.addElement("vidmate");
        defaultComboBoxModel2.addElement("9APPS");
        boxChannel.setModel(defaultComboBoxModel2);
        panel.add(boxChannel, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(30, 20), new Dimension(100, 40), new Dimension(150, 60), 0, false));
        button = new JButton();
        button.setText("确定修改");
        panel.add(button, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, new Dimension(20, 15), new Dimension(70, 30), new Dimension(80, 60), 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel;
    }
}
